package tddPractice;

import java.util.Arrays;

public class StringCalculator {
    public int add(String numbers) {
        if (numbers.contains(",\n")) {
            throw new NumberFormatException();
        }
        return numbers.equals("") ? 0 : Arrays.stream(numbers.split("[,|\\n]")).mapToInt(Integer::parseInt).sum();
    }
}
