package tddPractice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

public class StringCalculatorTest {
    @Test
    void canReceiveEmptyString() {
        StringCalculator stringCalculator = new StringCalculator();
        final int EXPECTED_RESULT = 0;
        final int ACTUAL_RESULT = stringCalculator.add("");
        Assertions.assertEquals(EXPECTED_RESULT, ACTUAL_RESULT);
    }

    @Test
    void canReceiveOneNumber() {
        StringCalculator stringCalculator = new StringCalculator();
        final int EXPECTED_RESULT = 0;
        final int ACTUAL_RESULT = stringCalculator.add("0");
        Assertions.assertEquals(EXPECTED_RESULT, ACTUAL_RESULT);
    }

    @Test
    void canReceiveDifferentNumbers() {
        StringCalculator stringCalculator = new StringCalculator();
        final int MAX_TEST_NUMBER = 50;
        for (int i = 0; i <= MAX_TEST_NUMBER; i++) {
            Assertions.assertEquals(i, stringCalculator.add(String.valueOf(i)));
        }
    }

    @Test
    void canReceiveTwoNumbers() {
        StringCalculator stringCalculator = new StringCalculator();
        final int EXPECTED_RESULT = 0;
        final int ACTUAL_RESULT = stringCalculator.add("0,0");
        Assertions.assertEquals(EXPECTED_RESULT, ACTUAL_RESULT);
    }

    @Test
    void canReceiveTwoDifferentNumbers() {
        StringCalculator stringCalculator = new StringCalculator();
        final int MAX_TEST_NUMBER = 50;
        for (int i = 0; i <= MAX_TEST_NUMBER; i++) {
            for (int j = MAX_TEST_NUMBER; j >= 0; j--) {
                Assertions.assertEquals((i + j), stringCalculator.add(i + "," + j));
            }
        }
    }

    @Test
    void canReceiveUnknownAmountOfNumbers() {
        StringCalculator stringCalculator = new StringCalculator();
        String numbers = Arrays.toString(IntStream.range(1, 100).toArray()).replaceAll("[\\[\\]\\s+]", "");
        final int EXPECTED_RESULT = IntStream.range(1, 100).sum();
        final int ACTUAL_RESULT = stringCalculator.add(numbers);
        Assertions.assertEquals(EXPECTED_RESULT, ACTUAL_RESULT);
    }

    @Test
    void canHandleNewLines() {
        StringCalculator stringCalculator = new StringCalculator();
        final String NUMBERS = "1\n2,3";
        final int EXPECTED_RESULT = 6;
        final int ACTUAL_RESULT = stringCalculator.add(NUMBERS);
        Assertions.assertEquals(EXPECTED_RESULT, ACTUAL_RESULT);
    }

    @Test
    void doesNotAllowWronglyPlacedNewLine() {
        StringCalculator stringCalculator = new StringCalculator();
        final String NUMBERS = "1,\n";
        Assertions.assertThrows(NumberFormatException.class, () -> stringCalculator.add(NUMBERS));
    }
}
